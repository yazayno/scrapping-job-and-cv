from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time
import re
import uuid
import os
import sys

chrome_options = Options()
# chrome_options.add_argument("--incognito")
chrome_options.add_argument("--window-size=1920x1080")
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument('--ignore-ssl-errors=yes')
chrome_options.add_argument('--ignore-certificate-errors')


# chrome_options.add_argument('--user-data-dir=C:/Users/Acer/AppData/Local/Google/Chrome/User Data')
# chrome_options.add_argument('--profile-directory=Profile 1'),
# chrome_options.add_argument('--headless')
# open document and start writing process


def check_job_is_today(date_str):
    return True
    # regex_list = ["second ago",
    #               "seconds ago",
    #               "minute ago",
    #               "minutes ago",
    #               "hour ago",
    #               "hours ago"]
    # for regex in regex_list:
    #     if regex in date_str:
    #         return True
    # return False


keyword = input("Keyword(str): ")
amount = int(input("Amount(int): "))

url = f'https://www.upwork.com/nx/jobs/search/?q={keyword}&sort=recency'
driver = webdriver.Chrome(options=chrome_options, executable_path="chromedriver", )
driver.get(url)
time.sleep(2)
data_counter = 0

base_dir = os.getcwd()

if keyword == "":
    job_desc_folder_path = os.path.join(base_dir, 'job_files', "general")
else:
    job_desc_folder_path = os.path.join(base_dir, 'job_files', keyword)

if not os.path.exists(job_desc_folder_path):
    os.makedirs(job_desc_folder_path)

while True:

    # wait content to be loaded
    WebDriverWait(driver, 20).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, "h3[class='my-0 p-sm-right job-tile-title'] > a")))

    # detect next button
    nextButton = driver.find_elements(By.CSS_SELECTOR,
                                      "button[class='up-pagination-item up-btn up-btn-link'] > div[class='next-icon up-icon']")

    # take web element
    jobTitle = driver.find_elements(By.CSS_SELECTOR, "h3[class='my-0 p-sm-right job-tile-title'] > a")
    jobDesc = driver.find_elements(By.CSS_SELECTOR, "span[data-test='job-description-text']")
    jobPostedDate = driver.find_elements(By.CSS_SELECTOR, "span[data-test='UpCRelativeTime']")
    jobExperienceLevel = driver.find_elements(By.CSS_SELECTOR, "span[data-test='contractor-tier']")

    for el in range(len(jobTitle)):

        if check_job_is_today(jobPostedDate[el].text):
            data_counter += 1
            file_path = os.path.join(job_desc_folder_path, f'job_{keyword}_{data_counter}.txt')

            with open(file_path, 'w', encoding="utf-8") as f:
                f.write(f'{jobTitle[el].text} \n{jobDesc[el].text}')

            if data_counter >= amount:
                sys.exit("Finish")
        else:
            print("Yarın oldu")
    # detect next button disabled
    if (len(nextButton) == 0):
        sys.exit("Finish")

    # move to next page
    nextButton[0].click()
    time.sleep(3)