import time
import re
import uuid
import os
import sys
import requests
import json
import tqdm

keyword = input("Keyword(str): ")
amount = int(input("Amount(int): "))

base_dir = os.getcwd()

if keyword == "":
    cv_desc_folder_path = os.path.join(base_dir, 'cv_files', "general")
else:
    cv_desc_folder_path = os.path.join(base_dir, 'cv_files', keyword)


if not os.path.exists(cv_desc_folder_path):
    os.makedirs(cv_desc_folder_path)

offset = 0
data_counter = 0
while True:

    url = f'https://www.freelancer.com/ajax/directory/getFreelancer.php?offset={offset}&freeSearch={keyword}'
    offset += 10

    response = requests.get(url)
    response = response.json()

    if response["status"] == "success":
        users = response["users"]
        for user in users:
            data_counter += 1
            cvDesc = user["user_profile"]
            # skillDesc = "Skills:"
            # for skill in user["skills"]:
            #     skillDesc += f"\n{str(skill['skill_name'])}"
            #     print(skill['skill_name'], type(skill['skill_name']))

            file_path = os.path.join(cv_desc_folder_path, f'cv_{data_counter}.txt')
            with open(file_path, 'w', encoding="utf-8") as f:
                # f.write(f'{cvDesc} \n{skillDesc}')
                f.write(f'{cvDesc}')

            if data_counter >= amount:
                sys.exit("Finish")
    else:
        sys.exit("Finish")
